#! /usr/bin/env ruby
#  :set status_display_program= in cmus

#  displays cmus status for dwm-bar
class CmusWidget
  def artist
    parse('tag artist', '\w.+')
  end

  def song
    parse('tag title', '\w.+')
  end

  def status
    `top | grep cmus`.empty? || `cmus-remote -Q`.match?('stopped')
  end

  def parse(position, regex)
    `cmus-remote -Q`.match(/#{position}\s(#{regex})/).captures[0]
  end

  def update
    status ? '' : " #{artist}: #{song} |"
  end
end
