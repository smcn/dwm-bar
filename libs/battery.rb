# displays battery information on Linux
class LinuxBatteryWidget
  attr_reader :bat0_max, :bat1_max, :bat0_cur, :bat1_cur
  def initialize
    @bat0_max = File.read('/sys/class/power_supply/BAT0/energy_full')
                    .delete('\n').to_f
    @bat1_max = File.read('/sys/class/power_supply/BAT1/energy_full')
                    .delete('\n').to_f
  end

  def update_current
    @bat0_cur = File.read('/sys/class/power_supply/BAT0/energy_now')
                    .delete('\n').to_f
    @bat1_cur = File.read('/sys/class/power_supply/BAT1/energy_now')
                    .delete('\n').to_f
  end

  def max_charge
    @bat0_max + @bat1_max
  end

  def current_charge
    @bat0_cur + @bat1_cur
  end

  def percentage
    ((current_charge / max_charge) * 100.0).to_s + '%'
  end

  def update
    update_current
    if File.read('/sys/class/power_supply/AC/online').delete('\n').to_i == 1
      percentage.delete('%').to_i > 94 ? 'Full' : 'Charging'
    else
      percentage
    end
  end
end

# displays battery information on OpenBSD
class OpenBSDBatteryWidget
  def current_charge
    `apm -l`
  end

  def update
    if `apm -a`.to_i == 1
      current_charge.to_i > 93 ? 'Full' : "Charging(#{current_charge.strip.concat('%')})"
    else
      current_charge.strip.concat('%')
    end
  end
end
