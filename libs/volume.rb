#! /usr/bin/env ruby

# updates volume widget on Linux
class LinuxVolumeWidget
  def data
    `amixer get Master`
  end

  def status
    data.match(/\[\w*\]/).to_s == '[on]'
  end

  def update
    status ? data.match(/\d*%/).to_s : 'Muted'
  end
end

# updates volume widget on OpenBSD
class OpenBSDVolumeWidget
  def current_volume
    `mixerctl -a`.split.reverse.each do |line| 
      return line.split(',')[-1] if line =~ /outputs\.master=/
    end
  end
  
  def muted?
    `mixerctl -a`.match?(/outputs.master.mute=on/)
  end
  
  def update
    muted? ? 'Muted' : current_volume
  end
end
