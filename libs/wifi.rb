#! /usr/bin/env ruby
# -*- coding: utf-8 -*-

# prints out current networkname using networkmanager
class NetworkManagerWifiWidget
  def network_name
    if set_status
      networkname = ''
      while networkname.empty?
        data = `nmcli c | rg wlp4s0`.split
        networkname = data[0..(data.length - 4)].join(' ')
      end
      networkname
    else
      'Offline'
    end
  end

  def set_status
    /on/.match?(`wifi`)
  end

  def update
    network_name.to_s
  end
end

# prints out current networkname using OpenBSD
class OpenBSDWifiWidget
  def ifconfig
    `ifconfig`.split(/\n\w/)
  end

  def wireless_interface
    ifconfig[1]
  end

  def wireless_status
    wireless_interface.to_s =~ /status:\sactive/
  end

  def wireless_connection_name
    wireless_interface.match(/nwid\s(.*?)\schan/)[1].gsub(/"/, '')
  end

  def wired_interface
    ifconfig[2]
  end 

  def wired_status
    wired_interface.to_s =~ /status:\sactive/
  end

  def update
    if wired_status
      'Ethernet'
    elsif wireless_status
      wireless_connection_name
    else
      'Offline'
    end
  end
end
