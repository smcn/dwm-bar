#! /usr/bin/env ruby
# Outputs data to dwm's bar

require 'date'
require_relative 'libs/wifi'
require_relative 'libs/volume'
require_relative 'libs/battery'
require_relative 'libs/cmus'

# outputs information to the dwmbar
class DwmBar
  attr_reader :time, :volume, :wifi, :battery, :cmus
  def initialize(volume, wifi, battery, cmus)
    @volume  = volume
    @wifi    = wifi
    @battery = battery
    @cmus    = cmus
  end

  def update_time
    Time.now.strftime('%H:%M | %a %0d/%0m/%Y')
  end

  def update_volume
    @volume.update
  end

  def update_wifi
    @wifi.update
  end

  def update_battery
    @battery.update
  end

  def update_cmus
    @cmus.update
  end

  def redraw
    `xsetroot -name "#{update_cmus} Vol: #{update_volume}\
 | Bat: #{update_battery}\
 | #{update_wifi} | #{update_time} "`
  end

  def count_down
    loop do
      redraw
      sleep(30)
    end
  end
end

bar = DwmBar.new(
  OpenBSDVolumeWidget.new,
  OpenBSDWifiWidget.new,
  OpenBSDBatteryWidget.new,
  CmusWidget.new
)

case ARGV[0]
when 'start'
  bar.count_down
else
  bar.redraw
end

bar.redraw if ARGV[0].nil?
